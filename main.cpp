#include <iostream>
#include <algorithm>
#include <iterator>
#include <bits/stdc++.h>

using namespace std;



//------------------------------------------------ UTILITY FUNCTIONS -----------------------------------------------

// Create a nxn incidence matrix with weights a_kl = (ak+bl)^2 + 1 ,when k<l
int** generateGraph(int a, int b, int n) {

    int** graph = new int*[n];
    for(int i = 0; i < n; ++i) graph[i] = new int[n];

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (i == j) {
                graph[i][j] = 0;
            } else if (i < j) {
                graph[i][j] = (a*(i+1)+b*(j+1))*(a*(i+1)+b*(j+1)) + 1;
            } else {
                graph[i][j] = (b*(i+1)+a*(j+1))*(b*(i+1)+a*(j+1)) + 1;
            }
        } 
    }
    return graph;
}

// Compute path cost from int array  1234 > sum of weights : 12,23,34,41
int computePathCost(int* path, int** graph, int numOfVertices){
    int cost = 0;

    for (int i = 1; i < numOfVertices; ++i) {
        int from = path[i-1];
        int to = path[i];
        cost += graph[from][to];
    }

    int last = path[numOfVertices-1];
    int first = path[0];
    return cost + graph[last][first];
}

// swap arr elements
void swap(int* seq, int i, int j) {
    int tmp = seq[i];
    seq[i] = seq[j];
    seq[j] = tmp;
}

// first element is fixed, n-1 elements are permutated
int getFirst(const int* seq, int size){
    for (int i = size - 2; i >= 0; --i) if (seq[i] < seq[i+1]) return i;
    return -1;
}

// TODO get distinct hamiltonian cycles
bool nextPerm(int* seq, int size){
    int first = getFirst(seq,size);
    if(first == -1) return false;
    int toSwap = size - 1;
    while(seq[first] >= seq[toSwap]) --toSwap;
    swap(seq, first++, toSwap);
    toSwap = size - 1;
    while(first < toSwap) swap(seq, first++, toSwap--);
    return true;
}

void printPermutations(int n){
    int count = 0;
    int* perm = new int[n];
    for (int i = 0; i < n; ++i) perm[i] = i;
    do {
        for (int i = 0; i < n; ++i) cout << perm[i] << " ";
        cout << endl;
        count++;
    } while (nextPerm(perm, n));
    cout << "count:" << count;
}

//------------------------------------------------ UTILITY FUNCTIONS -----------------------------------------------

void walk(int** graph, int size, int currIndex, vector<int> path, vector<bool> visited, int &ans){

    bool exit = true;
    for (int i = 0; i < size; ++i) {
        exit = visited[i] && exit;
    }
    if(exit){
        path.push_back(path[0]);
        int cost = 0;
        for(int i=0; i < path.size()-1; i++){
            cost += graph[path[i]][path[i+1]];
        }
        if(cost < ans){
            ans = cost;
        }
    }

    // find local unvisited minimum
    int minWeight = INT_MAX;
    for (int i = 0; i < size; ++i) {
        if(!visited[i] && graph[currIndex][i] < minWeight && i != currIndex){
            minWeight = graph[currIndex][i];
        }
    }

    for (int i = 0; i < size; ++i) {
        if(!visited[i] && graph[currIndex][i] == minWeight && i != currIndex){
            path.push_back(i);
            visited[i] = true;
            walk(graph, size, i, path, visited, ans);
        }
    }

}

// TODO Greedy algo
void GreedyTsp(int** graph, int size, int &ans)  {

    int minWeight = INT_MAX;
    // find global minimum
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if(graph[i][j] < minWeight && i!=j) {
                minWeight = graph[i][j];
            }
        }
    }

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if(minWeight == graph[i][j] && i!=j){
                vector<int> path;
                vector<bool> visited(size);
                for (int k = 0; k < size; k++)
                    visited[i] = false;
                path.push_back(i);
                visited[i] = true;
                walk(graph, size, i, path, visited, ans);
            }
        }
    }

}



// go through all n! permutations
// func util funcs: computePathCost(), nextPerm()
int BruteForceTsp1(int** graph, int size) {

    // Generiraj prvi poredak vrhova
    int* perm = new int[size];
    for (int i = 0; i < size; ++i) perm[i] = i;

    // varijabla koja se mjenja kako nalazimo krace puteve
    int shortestPathCost = INT_MAX;

    do {
        int pathCost = computePathCost(perm, graph, size);
        if (pathCost < shortestPathCost) {
            shortestPathCost = pathCost;
        }
    } while (nextPerm(perm, size));

    return shortestPathCost;
}

// go through all n! permutations using <algo> lib
int BruteForceTsp2(int** graph, int start, int size) {
    vector<int> vertex;
    for (int i = 0; i < size; i++)
        if (i != start)
            vertex.push_back(i);
    int minPath = INT_MAX;
    do {
        int currentPathWeight = 0;
        int k = start;
        for (int i = 0; i < vertex.size(); i++) {
            currentPathWeight += graph[k][vertex[i]];
            k = vertex[i];
        }
        currentPathWeight += graph[k][start];
        minPath = min(minPath, currentPathWeight);
    } while (next_permutation(vertex.begin(), vertex.end()));

    return minPath;
}

// https://www.geeksforgeeks.org/travelling-salesman-problem-implementation-using-backtracking/?ref=lbp
void BacktrackingRecursiveTsp(int** graph, int n, vector<bool>& v, int currPos, int count, int cost, int& ans){

    if (count == n && graph[currPos][0]) {
        ans = min(ans, cost + graph[currPos][0]);
        return;
    }

    for (int i = 0; i < n; i++) {
        if (!v[i] && graph[currPos][i]) {
            // Mark as visited
            v[i] = true;
            BacktrackingRecursiveTsp(graph, n, v, i, count + 1,
                cost + graph[currPos][i], ans);

            // Mark ith node as unvisited
            v[i] = false;
        }
    }
}

int RecursiveDynamicTsp(int startNode, int** graph) {

}

int main() {

    int a = 4;
    int b = -3;
    int n = 10; // Mora biti >=3 ! dodaj provjeru nakon upisivanja varijable
    auto* graph = generateGraph(a,b,n);

    cout << "BruteForceTsp1: " << BruteForceTsp1(graph, n) << endl;
    cout << "BruteForceTsp2: " << BruteForceTsp2(graph, 0, n) << endl;

    // Boolean array to check if a node
    // has been visited or not
    vector<bool> v(n);
    for (int i = 0; i < n; i++)
        v[i] = false;
    // Mark 0th node as visited
    v[0] = true;
    int ans = INT_MAX;
    BacktrackingRecursiveTsp(graph, n, v, 0, 1, 0, ans);
    cout << "Backtracking: " << ans << endl;

    ans = INT_MAX;
    for (int i = 0; i < n; i++)
        v[i] = false;
    ans = INT_MAX;
    GreedyTsp(graph, n, ans);
    cout << "Greedy: " << ans;

    //printPermutations(n);

    return 0;
}